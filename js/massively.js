$(document).ready(function() {
    //Menu and navigation content toggling
    $("nav ul li").click(function() {
     $("nav ul li").removeClass("nav-active");
        $(this).addClass("nav-active");       
        var x = $(this).index();
        $(".nav-content").hide().eq(x).show();    
    });

     //Navigation icon functionality
     $(".nav-group").click(function() {
        $(".navigation").css("display","block");
        $(".navigation").css("right","0");
     });
     $(".close-cross").click(function() {
        $(".navigation").css("right","-100%");
     });

});//end of document ready